# Spotify light

Spotify light ist eine Audio Streaming Software basierend auf einer Microservice-Architektur.

Aktuelle Features:
* User registieren
* Song streamen
* Song uploaden
* PLaylisten erstellen

## Übersicht 

Ein neuer User registriert sich über das Frontend und erhält vom **Userservice** ein JWT-Web-Token. <br>
Dieser Token wird für die Authentifizierung und Authorisierung bei allen Services verwendet.

Alle Requests vom Frontend werden über ein **API-Gateway** geroutet.

Ein User mit Administrator Rechte, kann neue Songs uploaden. <br>
Der **Songservice** nimmt das Songbinary entgegen und sendet das Binary zum **Mediaservice** für die Speicherung. <br>
Dieser gibt nach erfolgreicher Speicherung die ID zurück. unter der der Song abgespeichert wurde. <br>
Der **Songservice** speichert die ID zusammen mit Daten wie, Name, Interpret, Genre, etc. in der eigenen Datenbank.

Wenn ein Song abgespielt wird, wird ein Request an den **Streamingservice** gesendet. <br>
Der angeforderte Song wird vom **Streamingservice** über die Song ID beim **Mediaservice** angefragt. <br>
Dieser gibt als Response den Song als Base64 Format zurück. <br>
Sobald der **Streamingservice** die Datei hat, extrahiert der Service den ensprechenden Teil des Songs <br>
(z.B.: Ein User möchte einen Song ab einer bestimmten Sekunden abspielen) und sendet diesen Abschnitt an das Frontend.<br>
Wird ein Song von Beginn an abgespielt, sendet der **Streamingservice** den kompletten Song an das Frontend.

Legt ein User im Frontend eine Playlist an, wird ein Request an den **Playlistservice** gesendet. <br>
Dieser erstellt unter der User ID, eine Playlist in der eigenen Datenbank. <br>
User haben die Möglichkeit, mehrere Playlisten anzulegen.

Visueller Überblick der Abhängigkeiten und dem Zusammenspiel, zwischen den einzelnen Komponenten:
![alt text](https://i.imgur.com/tPnk7sm.jpeg "Detail View")

## API Gateway
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/api-gateway))

Der Api Gateway Service enthält keinen Programmcode, dieser Service ist der zentrale Zugangspunkt für alle Requests zu den Backend-Services. <br>
(Routet Anfragen vom Frontend, zum richtigen Service im Backend)

Unter: **“src/main/resources/application.yml”** findet man alle Services, an die das Api-Gateway einen Request weiterleiten kann.

## Playlist service
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/playlistservice))

Das Playlist Service des Projektes „Spotify Light“ ist ein Service, welcher über eine definierte REST-API Funktionen bereitstellt. Dieser ist für die gesamte Verwaltung der Playlists im System verantwortlich. Das betrifft sowohl die Speicherung der Daten als auch die Bereitstellung bzw. die Manipulation der Daten, unter Berücksichtigung der Berechtigungen von Usern.

## Service discovery
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/service-discovery))

Der Service Discovery Service enthält keinen Programmcode, dieser Service erkennt automatisch alle Dienste/Services im Rechnernetz, <br>
welche sich in unserer zentralen Registry (EurekaServer) registrieren.

## Song service
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/songservice))

Dieser Service stellt eine RESTFull API zur Verfügung über welche Songs abgerufen, erstellt, upgedatet und gelöscht werden können. <br>
Auf anderer Seite gibt es eine Kommunikationsschnittstelle (REST) zum Media Service, über welcher der hochgeladene Song als String in BASE64 kodiert übergeben wird. <br>
Im Fall einer Löschungsanforderung, werden die Metadaten in der lokalen Datenbank gelöscht und gleichzeitig Information an das Media Service weitergeleitet, die Songdatei zu entfernen. <br>
Die Metadaten des Songs werden in der Datenbank des Songservices verwaltet. Für die Speicherung liegt eine H2 Datenbank zu Grunde. 

## User service
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/sst-2020-bb-userservice))

Dieser Service stellt eine restful API zur Verfügung um User im System anzulegen, zu verändern,  <br>
zu löschen als auch eine JWT Authentifizierung. 

## Media service
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/sst-mediaservice))

Der Mediaservice ist für die Verwaltung der Binärdaten zuständig. <br>
Er nimmt Binärdaten als Base64 String über eine restful API an, konvertiert und speichert diese und liefert sie auf Anfrage wieder aus. 

## Streaming service
(**Url:** [Link zum Repository](https://gitlab.com/fhs-sst-itsb-2020/fhs-swt-steaming-service))

Der Streamingservice erhält vom Frontend einen Request mit einer Song ID und einer Byterange (z.B. 0 bis 1000, 1000 bis 2000, etc.). <br>
Der Streamingservice sendet einen Request mit der Song ID zum Mediaservice, der den Song, im Base64 Format als Response zurückschickt. <br>
Danach wird der Song im Bas64 Format, in ein Bytearray umgewandelt und die requestet Byterange von dem Bytearray extrahiert. <br>
Das extrahierte Bytearray wird als Respone an das Frontend geschickt.<br> 
Wenn keine Byterange spezifiziert wurde, wird der gesamte Song an das Frontend geschickt. 


